# SOA 2019

Data and code used for presenting at the 2019 SOA Annual Conference for session #118, "Machine Learning Theory and Real-World Considerations".

## Downloading the data and scripts

Don't feel the need to download the data (or clone the entire repository). The scripts use the GitLab URL to access the data.

If you are having difficulty cloning the repository yourself then you can use the "Download" button (it looks like an arrow pointing down in a box) to get a .zip file with all the files.